/*
What is the alternative to callbacks?
Info
In general callback function is defined as a function, that gets passed as an argument to another function, and is executed after some of that is happens. A callback function is called at the completion of a given task. 
*/
var getUser = (id, callback) => {
// dummy data
	var user = {
		name: "Krste",
	};
	
//	callback(user); // init
	
	setTimeout(()=>{
		callback(user);
		//I could have another line right here.
		//This is a more obvious example but it could easily be hidden inside of complex if else statements.
		//callback(); // big problems for our program.
	},3000);
};

//We're going to expect that user object things like ID, name, email or whatever comes back, as an argument to the callback function then and here i can actually do something with that data.(in our case i can  simple print it)
getUser(123, (userObject)=>{ 
	console.log(userObject.name);
}) 



/*
Alternative to callback and best solution is to use async.

Promises (alternative to callbacks) is that a promise represents the result of an asynchronous operation, greatly simplify the code and understanding data flow.
Promises aim to solve a lot of the problems that come up when you have a lot of asynchronous code in your application and then make it a lot easier to manage your asynchronous computation. Things like request data from DB or other type of fetching data.
*/
var somePromise = new Promise((resolve, reject) => {
//You have to handle errors inside of yours node application.
//In that case you wouldn't call resolve, you would call reject.
  setTimeout(() => {
    resolve('Hey. It worked!');
	//It is never going to get called twice. No matter how many times you try to call resolver or reject this function is only going to get fired once
	
    //reject('Unable to fulfill promise');
	
    //resolve('resolve!');
    //reject('reject!');
  }, 2500);
});

somePromise.then((message) => {
  console.log('Success: ', message);
}, (errorMessage) => { // second argument event; This is what lets us handle errors in our promises.
  console.log('Error: ', errorMessage);
});

/*
We have a function that handles that if it gets rejected or handle that as well(resolved).
You're going to provide different function depending on whatever or not the promise got resolved the rejected. This lets you avoid a lot of the complex if statements inside of your code which we needed to do in. Now inside of promise its important to understand that you can only either resolve or reject a promise once, if you resolve a promise you can't reject it later. And if you resolve that with one value you can't change your mind at a later point in time. There's nothing preventing us from accidentally calling the callback function twice.
This is another great advantage over callbacks. 
*/
