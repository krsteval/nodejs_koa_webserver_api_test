Promise.resolve(1)
  .then((x) => x + 1)

  .then((x) => { throw new Error('My Error') })

  .catch(() => 1)

  .then((x) => x + 1)

  .then((x) => console.log(x))

  .catch(console.error);

/*
  Output will be always 2 (two)
  Track on 'then' passing in our callbacks
  In practice, it is often desirable to catch rejected promises, like this case, rather than use then's two case syntax.
  Line in number 6 with catch function is allways reset value of parameter in resolve() method = 1, It's always called even if the promise is rejects
  In our cases, a resolving promise, the first then() will return 2, baset on inuput value, who is now 1.
  Second then() function throw an error, an then catch() function is reset value on 1.
  Third conditional then() is increment value again and 1+1 = 2 is 2.
  Fourth then() function is printing input on console and this is the final value, who was incremented in third then() and final value is 2.
*/