import router from 'koa-router'; 
import user from '../users/'; 

//Router middleware for koa
// Set first router to handle response where user is on http://localhost:3000/user
const indexRouter = new router();

// Response
indexRouter.get('/', async (ctx, next) => {
  ctx.body = { 
    user: 'GET /user' 
  };

  await next();
});

indexRouter.use('/user', user.router.routes(), user.router.allowedMethods());

export default indexRouter;