import Sequelize from 'sequelize';

// Database connection options 
//database config
import dbConfig from '../../config/db.json'; 
const currentDb = dbConfig[process.env.NODE_ENV || 'dev'];

let connection = new Sequelize(currentDb); 

// Format model to sq as JSON
//  MODELS
const models = {
  User: connection.import('../users/models')
};

//A Model represents a table in the database.
Object.keys(models).forEach((modelName) => {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models);
  }
});

connection.sync({ force: false });

models.connection = connection;
models.Sequelize = Sequelize;

export default models;