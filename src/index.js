import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import helmet from 'koa-helmet'; 
import cors from 'koa-cors';
import logger from 'koa-logger';
import corsError from 'koa-cors-error';

import indexRouter from './index/indexRouter'; 

/*
    Server Config
*/
const app = new Koa();

// Logging
app.use(logger());

//Enable cors with default options
//Request Origin header
//Allow Methods
app.use(cors({
  origin: true,
  expose: [
    'Content-Type'
  ],
  methods: [
    'OPTIONS',
    'GET',
    'PUT',
    'POST',
    'PATCH',
    'HEAD',
    'DELETE'
  ]
}));

// Error handling
app.use(corsError);
 

// Body parsing
app.use(bodyParser({
  enableTypes: ['json', 'text']
  })
);

//koa-helmet is a wrapper for helmet to work with koa. It provides important security headers to app more secure.
app.use(helmet());
 
//Configure Router
app
  .use(indexRouter.routes())
  .use(indexRouter.allowedMethods());

// Error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    ctx.status = e.status || 500;
    ctx.body = `${e.message} \n`;
    ctx.app.emit('error', e, ctx);
  }
});

//Create and return an HTTP server. The following is a useless Koa application bound to port 3000
//Should be able to access http://localhost:3000/ in your browser
app.listen(3000);