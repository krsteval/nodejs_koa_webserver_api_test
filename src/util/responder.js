const standard = async (ctx, next) => {
    ctx.status = ctx.status || 200;
    ctx.body = ctx.body || '';
    await next();
  };
  
export default standard;