import hat from 'hat'; 

import db from '../models/index'; 
 
const Users = db.User;
 
const userIdsValidation = async (id: string) => {
   
    const userRecords: Array = await Users.findAll();
    const userIds = userRecords.map(u => u.id);
    return userIds.includes(id);
  
}
const getUser = async (ctx, next) => {
  try {
    const { id }: { id: string } = ctx.params;
    const validataionUserId = await userIdsValidation(id);
    if (!validataionUserId) {
      ctx.throw(`User id ${id} not valid`, 400);
    }
    const result = await Users.find({ where: { id: id } });
    ctx.body = result;
  } catch (e) {
    ctx.throw(e, 400);
  }
  await next();
}

const listUsers = async (ctx, next) => {
  const users: Array = await Users.findAll();
  ctx.body = users;
  await next();
}

const createUser = async (ctx, next) => {
  try {
    const {
      username,
      email, 
      profile
      }: {
        username: string,
        email: string, 
        profile: ?Object
      } = ctx.request.body;
      
    const res: Array = await Users.findAll({ where: { username: username } });

    if (res.length > 0) {
      ctx.throw(`Username ${username} is already exists`, 400);
    } else {
      const emailCheck: Array = await Users.findAll({ where: { email: email } });

      if (emailCheck.length > 0) {
        ctx.throw(`Email ${email} already exists`, 400);
      }

      // Generate random IDs and avoid collisions.
      const userId = hat(); 

      await Users.create({
        id: userId,
        username,
        email
      });

      ctx.status = 201;
    }
  } catch (e) {
    ctx.throw(e, 400);
  }
  await next();
}
 
const deleteUser = async (ctx, next) => {
  const { id }: { id: string } = ctx.params;
  const validataionUserId = await userIdsValidation(id);
  if (!validataionUserId) {
    ctx.throw(`User id ${id} not valid`, 400);
  }
  await Users.destroy({ where: { id: id } });
  ctx.status = 202;

  await next();
}
 

export default {
  listUsers,
  getUser,
  createUser, 
  deleteUser, 
};