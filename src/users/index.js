import actions from './actions';
import router from './router';
import model from './models';

export default {
  actions,
  router,
  model
};