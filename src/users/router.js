import router from 'koa-router';

//methods implementation of model User
import * as Actions from './actions';

//default respond status 200, for success request
import * as responder from '../util/responder'

//Router initialization
const userRouter = new router();
 
//Configure Route paths will be translated to regular expressions
const index = async (ctx, next) => {
  ctx.body = {
    user_detail: {
      list_users: 'GET /getall',
      get_list: 'GET /current/:id',
      create_user: 'POST /create',
      delete_user: 'DELETE /current/:id'
    }
  };
  await next();
}
userRouter.get('/', index);

const actions = Actions.default;
const standard = responder.default;

// Users routes/actions
//A set of routes, ie the components responsible for handling the incoming requests. 
userRouter.get('/current/:id', actions.getUser, standard);
userRouter.get('/getall/', actions.listUsers, standard);
userRouter.post('/create/', actions.createUser, standard); 
userRouter.delete('/current/:id', actions.deleteUser, standard); 

export default userRouter;