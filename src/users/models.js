import sequelize from 'sequelize';
/*
    User Schema
*/
export default (sequelize, DataTypes) => {
  // Define models for object User with set of definition for attributes
  const User = sequelize.define(
    'users',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      username: {
        type: DataTypes.STRING,
        validate: {
          isAlphanumeric: {
            args: true,
            msg: 'The username can only contain letters and numbers',
          },
          len: {
            args: [8, 25],
            msg: 'The username needs to be between 8 and 25 characters long',
          },
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          isEmail: {
            args: true,
            msg: 'Invalid email',
          },
        },
      },  
      createdAt: { type: DataTypes.DATE }
    }
  );
  return User;
};