# NodeJS_Koa_WebServer_API_Test
Questions
Go to source and open Krste_NodeJS_Tests

Task: 
Implement web server in nodejs using your favorite HTTP framework and sqlite DB. 
The web server should manage list of users persistently.  
The server should provide interface to view list of users. The server should allow to add new user. When new user added , all the web clients should display immediately the added user in the list. 

*Websocket is not allowed

------------------------------------------

Git clone https://krsteval@bitbucket.org/krsteval/nodejs_koa_webserver_api_test.git

In root folder open terminal and type: 
`npm install`

In root folder open terminal and type: 
`npm run dev`

`Type in any browser http://localhost:3000`



#Crud Operations
```

GET    list_users: 'user/getall',


GET    get_list: 'user/current/:id',             


POST   create_user: 'user/create',


DELETE delete_user: 'user/current/:id'

```
List of routes can se on any time on http://localhost:3000/user
#Working with SQLite database (in this case sqlite3)
SQLite in-memory database.
`npm install sqlite3`

#Sequelize
Sequelize will setup a connection pool on initialization so you should ideally only ever create one instance per database if you're connecting to the DB from a single process. Sequelize is a promise-based ORM for Node.js. Sequelize has dozens of cool features like synchronization, association, validation, etc. Have support for SQLite.
`npm install sequelize`

#Nodemon
nodemon - For monitor for any changes in your node.js application and automatically restart server on code changes.
```npm install --save-dev nodemon```

#Description
## Why Koa ?? Why koa over express ??
Koa.js is a powerful server framework for Node.js to build efficient web applications and APIs. Koa.js efficiently uses generators to deal with callbacks and increase error-handling capabilities. With Koa use whatever middleware you want with it, is much more stable (as in how often code is changing, not how often it is crashing)..
Koa makes development far simpler than classic callback-style Node.js with Express and simplify error handling. Includes a lot of features like async/await from ES2017 and the awesome yarn package-manager to create modern JSON APIs. It exposes its own ctx.request and ctx.response objects instead of node's req and res objects. It also parses the response content and sets the Content-Type header based on the type of its body property. Koa support third party libraries for routing. For Koa we have koa-helmet available and the list goes on for Koa third party available libraries.
Finally, we can manually set the response headers and status code. 

#File structure
```
├── src
│   ├── index
│   │   ├── indexRouter.js - Default rute response config 
│   ├── models
│   │   ├── index.js - Database connection options and model represents in DB table
│   ├── users
│   │   ├── action.js - Methods who give response based on User model
│   │   ├── index.js  - export action, models and router
│   │   ├── models.js - Define models for object User with set of definition for attributes
│   │   ├── router.js - User routes/actions
│   ├── util
│   │   ├── responder.js - Global default response for request 
│   ├── index.js - running fail, http module for Node server
├── config
│   ├── db.json - sqllite config file 
├── package.json - npm packages
├── .babelrc - Use different configuration for different files during the same build
├── .eslintrc - ESLint config file 
```
#Database model User schema
```
User
{
    
    id text,
    
    username text not null,
    
    email text not null
}
```
#Response 
```
[
    {
        "id": "889fc4abcdf44c77501d650890b26e89",
        "username": "KrsteMoskov",
        "email": "krstemoskov@gmail.com",
        "createdAt": "2018-02-26T14:07:41.091Z",
        "updatedAt": "2018-02-26T14:07:41.091Z"
    }
]
 
``` 
#URL calls
```
#GET
Get all users from db
http://localhost:3000/user/getall

#GET
Get user by id from db
http://localhost:3000/user/current/:id

#POST
Create user in db
http://localhost:3000/user/create
body raw JSON(application/json):
{   
 "username": "KrsteMoskov",
 "email": "krstemoskov@gmail.com" 

}

#DELETE
http://localhost:3000/user/current/:id
 
```

## Dependencies
```
 "dependencies": {
    "hat": "0.0.3",
    "koa": "^2.5.0",
    "koa-bodyparser": "^4.2.0",
    "koa-cors": "0.0.16",
    "koa-cors-error": "0.0.2",
    "koa-helmet": "^3.3.0",
    "koa-logger": "^3.1.0",
    "koa-route": "^3.2.0",
    "koa-router": "^7.4.0",
    "koa-stylish": "^0.1.1",
    "sequelize": "^4.33.4",
    "sqlite3": "^3.1.13"
  }
```